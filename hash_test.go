package main

import (
	"crypto/x509"
	"encoding/pem"
	"github.com/stretchr/testify/assert"
	"testing"
)

var testKey = []byte(`-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCqZcT8DSX9ji/fdPNFDElHg/7olgZICF4vxZed4dQKRLVJDipX
nhWk4KlF0o9z5ZIRjuzOD7dJ2pFUPSeqgWoVUmRvIxauvjJGifuu2+WOIJX0xF2j
o/waLGBiKHJfN/ZFWh+4EIzGHQUpQIRw6xQ6letdP7mB+22Yv/SPCB4s6QIDAQg/
AoGAEhM9eGROav24GUs7ChKfkTu+eMIqISoePqy8auY6TI2QEXpinDdFoA2A9lOo
8l5Gw5EBQN8F88sM49JZn47WLQBNgulFusbmTjEsknlSgsbHVXeEumgGv7aj/pFN
7MSti3QCSxlItIOArGYqNXmiV4fuYxM0vvH0WQK7KJGRbx8CQQDY+eM8UdVG3w8y
uLdbzrgbHWmCNDthmQ2jiBRlFBKzNtqXoBj326t+3VXY8lge+5j/Uobvv+ciCJYX
wP0kbFcRAkEAyQtKIdrA5ol1eMbL+GO/MQ7v3x/k/nwm0VFbD8E3IvljrghbccRX
JjiBgn/yFfEBbFjAZpthVvjctUCaq8VoWQJBAKFMLC1PaUKVuJwrz+r/nZD/tQ/L
s5LrXoay/TB8wlL6KneJNL6/sLX8SyAqOU+uIkmtVwndd61PVXoBFz/q0L8CQAh5
3OF53SZomsw7aNL3j9MBkIG0Mmd/qWpvQuH2NzjjhishLnxrdT8SslY8up4ektcA
IqHiMsewRepMOS+SW18CQBurAuFjfNn08pD7VUlVdcxK1CbBRZXsZksHRAHxa+yg
As2ZQAgXhh2ZUWZM/e471qnSySRVph5tWx61o/3wgz8=
-----END RSA PRIVATE KEY-----`)

func TestHash(t *testing.T) {
	assert := assert.New(t)

	block, _ := pem.Decode(testKey)
	if block == nil {
		panic("")
	}
	if block.Type != "RSA PRIVATE KEY" {
		t.Fatal("fail")
	}
	k, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		t.Fatal(err)
	}
	str, err := rsaKeyToOnionAddress(k.PublicKey)
	assert.NoError(err)
	assert.Equal("mwludwxhasd6e4xm.onion", str)

}
