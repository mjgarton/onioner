package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/asn1"
	"encoding/base32"
	"fmt"
	"math/big"
	"net/http"
	_ "net/http/pprof"
	"regexp"
	"runtime"
	"strings"
	"time"
)

func rsaKeyToOnionAddress(rk rsa.PublicKey) (string, error) {
	type rsaPublicKey struct {
		N *big.Int
		E int
	}

	der, err := asn1.Marshal(rsaPublicKey{rk.N, rk.E})
	if err != nil {
		return "", err
	}
	h := sha1.New()
	if _, err := h.Write(der); err != nil {
		return "", err
	}
	sum := h.Sum(nil)

	str := base32.StdEncoding.EncodeToString(sum[0:10])
	return fmt.Sprintf("%s.onion", strings.ToLower(str)), nil
}

func main() {

	go func() {
		http.ListenAndServe(":6666", nil)
	}()

	keys := make(chan *rsa.PrivateKey, 16)

	for i := 0; i < runtime.NumCPU(); i++ {
		go genKeys(keys)
	}

	re := regexp.MustCompile("^foo")

	var addr string
	t := time.Now()
	var count int
	for !re.MatchString(addr) {
		priv := <-keys
		var err error
		addr, err = rsaKeyToOnionAddress(priv.PublicKey)
		if err != nil {
			panic(err)
		}
		count++
		if count%50 == 0 {
			fmt.Printf("rate : %v\n", time.Now().Sub(t)/time.Duration(count))
		}
	}
	fmt.Println(addr)
}

func genKeys(keys chan<- *rsa.PrivateKey) {
	for {
		priv, err := rsa.GenerateKey(rand.Reader, 1024)
		if err != nil {
			panic(err)
		}
		keys <- priv
	}
}
